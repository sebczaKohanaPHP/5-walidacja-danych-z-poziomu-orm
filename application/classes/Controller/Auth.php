<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Auth extends Controller_Welcome {

    public function action_login() {
	if(!Auth::instance()->logged_in()){
	if(Auth::instance()->login('admin', 'admin'));
	$this->redirect('/auth/status');
	}
    }
    
    public function action_logout() {
	$this->template->title = "Logowanie";
	
	if(Auth::instance()->logged_in()){
	    Auth::instance()->logout(TRUE);
	    $this->redirect('/auth/status');
	}
    }
    public function action_status(){
	$this->template->content = View::factory('zalogowany');
    }


}
