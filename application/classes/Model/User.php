<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_User extends Model_Auth_User {
    
 public function __construct($id = NULL) {
     parent::__construct($id);
 }
 
 protected $_has_many = array(
     'post'=>array()
 );
}